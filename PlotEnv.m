function PlotEnv

%% Set Positions
basePos = [0 0 -0.1];                                                         %protective cage position
LettuceBoxPos = [0.15 0.15 -0.05];                                             %circuit box postion                                                      %stage position
BunBaseBoxPos = [0 0.15 -0.05];                                      %bottom housing box position
BunCrownBoxPos = [0.15 -0.15 -0.05];                                         %top housing box position
FinishedBoxPos = [0 -0.15 -0.05];                                          %finished items box position
ketchupPos = [-0.001 -0.156 0.302];                                                       %assembly position
mustardPos = [0.3 -1.97 0.5]; 

 testPos = [0 0 0]; 

base = transl(basePos);                                                     %translate of protective cage position
LettuceBox = transl([0.15 0.15 -0.05]);                                         %translate of circuit box postion                                                  %translate of stage position
BunBaseBox = transl([0 0.15 -0.05]);                             %translate of bottom housing box position
BunCrownBox = transl([0.15 -0.15 -0.05]);                                   %translate of top housing box position
finishedBox = transl([0 -0.15 -0.05]);                                       %translate of finished items box position

%% plot cage
function  CagePlot()
  [f,v,data] = plyread('baseCol.ply','tri');
  vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ base(1,4),v(:,2) + base(2,4), v(:,3) + base(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  
  camlight;
  axis equal;
end

%% Plot lettuce box
function  LettuceBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ LettuceBox(1,4),v(:,2) + LettuceBox(2,4), v(:,3) + LettuceBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

%% plot BunBase box
function  BunBaseBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BunBaseBox(1,4),v(:,2) + BunBaseBox(2,4), v(:,3) + BunBaseBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end
%% plot bun crown box
function  BunCrownBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BunCrownBox(1,4),v(:,2) + BunCrownBox(2,4), v(:,3) + BunCrownBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end
%% plot finished box
function  FinishedBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ finishedBox(1,4),v(:,2) + finishedBox(2,4), v(:,3)+ finishedBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

%% plot stage
function StagePlot()
  [f,v,data] = plyread('stage.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ stage(1,4),v(:,2) + stage(2,4), v(:,3)+ stage(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

%% plot pcb
function  PcbPlot(posTrans)
  [f,v,data] = plyread('pcb.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ posTrans(1),v(:,2) + posTrans(2), v(:,3)+ posTrans(3),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end
%% plot housingTop
function  TopHousingPlot(posTrans)
  [f,v,data] = plyread('housingTop.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ posTrans(1),v(:,2) + posTrans(2), v(:,3)+ posTrans(3),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end
%% plot housingBottom
function  BottomHousingPlot(posTrans)
  [f,v,data] = plyread('housingBottom.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ posTrans(1),v(:,2) + posTrans(2), v(:,3)+ posTrans(3),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

 %% PLOT ENVIRONMENT    
     CagePlot();                             %plot cage
%     StagePlot();                            %plot stage
     LettuceBoxPlot();                       %plot circuit box
     BunBaseBoxPlot();                 %plot bottom housing box
     BunCrownBoxPlot();                    %plot top housing box
     FinishedBoxPlot();                      %plot fimished itmes box

end