function Assignment2
%% 
clf;
clc;
%% Creates the robots and plots them
function PlotRobots()
%% Creates the robots and plots them
L1 = Link('d',0.1,'a',0,'alpha',pi/2,'offset',pi,'qlim',[deg2rad(-135),deg2rad(135)])
L2 = Link('d',0,'a',0.137,'alpha',0,'offset',0,'qlim',[deg2rad(0),deg2rad(85)])
L3 = Link('d',0,'a',0.147,'alpha',0,'offset',0,'qlim',[deg2rad(-95),deg2rad(10)])
L4 = Link('d',0,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(0),deg2rad(180)])
L5 = Link('d',0.05,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)])


robot1 = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot')


robot1.base = transl(0.2, 0, 0);


workspace = [-0.5 0.5 -0.5 0.5 -0.1 0.5];                                               %Set the size of the workspace when drawing the robot        
scale = 0.4;                                                                %scale of plot
q = zeros(1,5);                                                             %initial orientation of robots

%       robot1.plot(q,'workspace',workspace,'scale',scale);
    robot1.teach(q,'workspace',workspace,'scale',scale);
hold on;
   
camlight;
end
    
%% Set Positions
basePos = [0 0 -0.1];                                                         %protective cage position
LettuceBoxPos = [0.15 0.15 -0.05];                                             %circuit box postion                                                      %stage position
BunBaseBoxPos = [0 0.15 -0.05];                                      %bottom housing box position
BunCrownBoxPos = [0.15 -0.15 -0.05];                                         %top housing box position
FinishedBoxPos = [0 -0.15 -0.05];                                          %finished items box position
ketchupPos = [-0.001 -0.156 0.302];                                                       %assembly position
mustardPos = [0.3 -1.97 0.5]; 

 testPos = [0.15 0.15 -0.05]; 

base = transl(basePos);                                                     %translate of protective cage position
LettuceBox = transl([0.15 0.15 -0.05]);                                         %translate of circuit box postion                                                  %translate of stage position
BunBaseBox = transl([0 0.15 -0.05]);                             %translate of bottom housing box position
BunCrownBox = transl([0.15 -0.15 -0.05]);                                   %translate of top housing box position
finishedBox = transl([0 -0.15 -0.05]);                                       %translate of finished items box position

%% plot cage
function  CagePlot()
  [f,v,data] = plyread('base.ply','tri');
    trisurf(f,v(:,1)+ base(1,4),v(:,2) + base(2,4), v(:,3) + base(3,4));
  colormap(parula(10));
  axis equal;
end

%% Plot lettuce box
function  LettuceBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
    trisurf(f,v(:,1)+ LettuceBox(1,4),v(:,2) + LettuceBox(2,4), v(:,3) + LettuceBox(3,4));
  colormap(parula(10));
  axis equal;
end

%% plot BunBase box
function  BunBaseBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
    trisurf(f,v(:,1)+ BunBaseBox(1,4),v(:,2) + BunBaseBox(2,4), v(:,3) + BunBaseBox(3,4));
  colormap(parula(10));
  axis equal;
end
%% plot bun crown box
function  BunCrownBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
    trisurf(f,v(:,1)+ BunCrownBox(1,4),v(:,2) + BunCrownBox(2,4), v(:,3) + BunCrownBox(3,4));
  colormap(parula(10));
  axis equal;
end
%% plot finished box
function  FinishedBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
    trisurf(f,v(:,1)+ finishedBox(1,4),v(:,2) + finishedBox(2,4), v(:,3)+ finishedBox(3,4));
  colormap(parula(10));
  axis equal;
end

%% plot stage
function StagePlot()
  [f,v,data] = plyread('stage.ply','tri');
    trisurf(f,v(:,1)+ stage(1,4),v(:,2) + stage(2,4), v(:,3)+ stage(3,4));
  colormap(parula(10));
  axis equal;
end

%% plot pcb
function  PcbPlot(posTrans)
  [f,v,data] = plyread('pcb.ply','tri');
    trisurf(f,v(:,1)+ posTrans(1),v(:,2) + posTrans(2), v(:,3)+ posTrans(3));
  colormap(parula(10));
  axis equal;
end
%% plot housingTop
function  TopHousingPlot(posTrans)
  [f,v,data] = plyread('housingTop.ply','tri');
    trisurf(f,v(:,1)+ posTrans(1),v(:,2) + posTrans(2), v(:,3)+ posTrans(3));
  colormap(parula(10));
  axis equal;
end
%% plot housingBottom
function  BottomHousingPlot(posTrans)
  [f,v,data] = plyread('housingBottom.ply','tri');
    trisurf(f,v(:,1)+ posTrans(1),v(:,2) + posTrans(2), v(:,3)+ posTrans(3));
  colormap(parula(10));
  axis equal;
end

%% sawyer 1 move function, Inputs - position to move to 
function t2s1 = Saw1Move(posTrans)
%Inputs - position to move to 
x = posTrans(1);
y = posTrans(2);
z = posTrans(3);
steps = 50;   
q = zeros(1,5);
q1 = robot1.getpos();                                                    %sawywer1 current position

    T2 = transl(x,y,z);                                      % Define a translation matrix            
    q2 = robot1.ikcon(T2,q);                                    % Use inverse kinematics to get the joint angles
    qMatrix12 = jtraj(q1,q2,steps);
    
    robot1.plot(qMatrix12);

end

%% Main Program Begins from here.
%% plot robots in initial position
   PlotRobots()                             
   robot1;
 %% PLOT ENVIRONMENT    
     CagePlot();                             %plot cage
%     StagePlot();                            %plot stage
     LettuceBoxPlot();                       %plot circuit box
     BunBaseBoxPlot();                 %plot bottom housing box
     BunCrownBoxPlot();                    %plot top housing box
     FinishedBoxPlot();                      %plot fimished itmes box
%     PcbPlot(LettuceBoxPos);                 %plot pcb
%     TopHousingPlot(BunCrownBoxPos);       %plot top housing
%     BottomHousingPlot(BunBaseBoxPos); %plot bottom housing

%% ASSEMBLY SIMULATION

    %move sawyer1 to circuit box and pick circuit board
      
      Saw1Move(BunBaseBoxPos) 
      Saw1Move(BunCrownBoxPos) 
      Saw1Move(LettuceBoxPos) 
      Saw1Move(FinishedBoxPos)  
      Saw1Move(ketchupPos)
      Saw1Move(mustardPos)

end