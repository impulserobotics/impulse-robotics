function CreateRobot(handles)
%% 
% clf;
% clc;
%% Creates the robots and plots them
L1 = Link('d',0.1,'a',0,'alpha',pi/2,'offset',pi,'qlim',[deg2rad(-135),deg2rad(135)])
L2 = Link('d',0,'a',0.137,'alpha',0,'offset',0,'qlim',[deg2rad(0),deg2rad(85)])
L3 = Link('d',0,'a',0.147,'alpha',0,'offset',0,'qlim',[deg2rad(-95),deg2rad(10)])
L4 = Link('d',0,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(0),deg2rad(180)])
L5 = Link('d',0.05,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)])

global robot1;
robot1 = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot')


robot1.base = transl(0.2, 0, 0);


workspace = [-0.5 0.5 -0.5 0.5 -0.1 0.5];                                               %Set the size of the workspace when drawing the robot        
scale = 0.4;                                                                %scale of plot
q = zeros(1,5);                                                             %initial orientation of robots
     %robot1.plot(q,'workspace',workspace,'scale',scale);
%    robot1.teach(q,'workspace',workspace,'scale',scale);
% plot(findall(0,'Tag','SueGUIAxes'),1,1,'*')
% plot(findall(0,'Tag','SueGUIAxes'));
    axes(handles.axes1); 
    cla;
  
robot1.plot(q,'workspace',workspace,'scale',scale);
hold on;
PlotEnv();
   
camlight;

end