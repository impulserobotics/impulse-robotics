L1 = Link('d',0.1,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-135),deg2rad(135)])
L2 = Link('d',0,'a',0.137,'alpha',0,'offset',0,'qlim',[deg2rad(0),deg2rad(85)])
L3 = Link('d',0,'a',0.147,'alpha',0,'offset',0,'qlim',[deg2rad(-10),deg2rad(95)])
L4 = Link('d',0,'a',0,'alpha',pi/2,'offset',0,'qlim',[deg2rad(-90),deg2rad(90)])
L5 = Link('d',0.1,'a',0,'alpha',0,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)])



robot1 = SerialLink([L1 L2 L3 L4 L5], 'name', 'DoBot');


robot1.base = transl(0, 0, 0);

workspace = [-2 2 -2 2 -2 2];                                               %Set the size of the workspace when drawing the robot        
scale = 0.5;                                                                %scale of plot.
q = zeros(1,5);                                                             %initial orientation of robots
robot1.teach(q);
% robot1.plot(q,'workspace',workspace,'scale',scale);
   
camlight;