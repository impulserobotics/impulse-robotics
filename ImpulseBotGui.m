function varargout = impulseBot(varargin)

% IMPULSEBOT MATLAB code for impulseBot.fig
%      IMPULSEBOT, by itself, creates a new IMPULSEBOT or raises the existing
%      singleton*.
%
%      H = IMPULSEBOT returns the handle to a new IMPULSEBOT or the handle to
%      the existing singleton*.
%
%      IMPULSEBOT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMPULSEBOT.M with the given input arguments.
%
%      IMPULSEBOT('Property','Value',...) creates a new IMPULSEBOT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before impulseBot_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to impulseBot_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help impulseBot

% Last Modified by GUIDE v2.5 07-Oct-2017 19:44:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @impulseBot_OpeningFcn, ...
                   'gui_OutputFcn',  @impulseBot_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before impulseBot is made visible.
function impulseBot_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to impulseBot (see VARARGIN)

% Choose default command line output for impulseBot
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes impulseBot wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = impulseBot_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

CreateRobot(handles);


% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  
MakeBurger();


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

set(handles.editq1,'string',num2str(get(hObject,'Value')));
SetqAngles(handles);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called 
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
  
  
end
             

             


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editq2,'string',num2str(get(hObject,'Value')));
SetqAngles(handles);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editq3,'string',num2str(get(hObject,'Value')));
SetqAngles(handles);


% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editq4,'string',num2str(get(hObject,'Value')));
SetqAngles(handles);


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    set(handles.editq5,'string',num2str(get(hObject,'Value')));
    SetqAngles(handles);


% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function xbox_Callback(hObject, eventdata, handles)
% hObject    handle to xbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xbox as text
%        str2double(get(hObject,'String')) returns contents of xbox as a double

      


% --- Executes during object creation, after setting all properties.
function xbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ybox_Callback(hObject, eventdata, handles)
% hObject    handle to ybox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ybox as text
%        str2double(get(hObject,'String')) returns contents of ybox as a double


% --- Executes during object creation, after setting all properties.
function ybox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ybox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zbox_Callback(hObject, eventdata, handles)
% hObject    handle to zbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zbox as text
%        str2double(get(hObject,'String')) returns contents of zbox as a double


% --- Executes during object creation, after setting all properties.
function zbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in moveButton.
function moveButton_Callback(hObject, eventdata, handles)
% hObject    handle to moveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
x = str2double(get(handles.xbox,'string'))
y = str2double(get(handles.ybox,'string'))
z = str2double(get(handles.zbox,'string'))
      movePos = [x y z]
      DobotMove(movePos);



function editq1_Callback(hObject, eventdata, handles)
% hObject    handle to editq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editq1 as text
%        str2double(get(hObject,'String')) returns contents of editq1 as a
%        double
set(handles.slider1,'Value',str2double(get(hObject,'String')));
SetqAngles(handles);



% --- Executes during object creation, after setting all properties.
function editq1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editq2_Callback(hObject, eventdata, handles)
% hObject    handle to editq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editq2 as text
%        str2double(get(hObject,'String')) returns contents of editq2 as a double
set(handles.slider2,'Value',str2double(get(hObject,'String')));
SetqAngles(handles);



% --- Executes during object creation, after setting all properties.
function editq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editq3_Callback(hObject, eventdata, handles)
% hObject    handle to editq3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editq3 as text
%        str2double(get(hObject,'String')) returns contents of editq3 as a double
set(handles.slider3,'Value',str2double(get(hObject,'String')));
SetqAngles(handles);



% --- Executes during object creation, after setting all properties.
function editq3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editq3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editq4_Callback(hObject, eventdata, handles)
% hObject    handle to editq4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editq4 as text
%        str2double(get(hObject,'String')) returns contents of editq4 as a double
set(handles.slider4,'Value',str2double(get(hObject,'String')));
SetqAngles(handles);



% --- Executes during object creation, after setting all properties.
function editq4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editq4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editq5_Callback(hObject, eventdata, handles)
% hObject    handle to editq5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editq5 as text
%        str2double(get(hObject,'String')) returns contents of editq5 as a double
set(handles.slider5,'Value',str2double(get(hObject,'String')));
SetqAngles(handles);


% --- Executes during object creation, after setting all properties.
function editq5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editq5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function SetqAngles(handles)
q1 = deg2rad(get(handles.slider1, 'Value'));
    q2 = deg2rad(get(handles.slider2, 'Value'));
    q3 = deg2rad(get(handles.slider3, 'Value'));
    q4 = deg2rad(get(handles.slider4, 'Value'));
    q5 = deg2rad(get(handles.slider5, 'Value'));
            
    jAngles = [q1 q2 q3 q4 q5];
    q = DobotMove(jAngles);
    set(handles.xbox,'string',num2str(q(1)));
    set(handles.ybox,'string',num2str(q(2)));
    set(handles.zbox,'string',num2str(q(3)));


% --- Executes on button press in estopButton.
function estopButton_Callback(hObject, eventdata, handles)
% hObject    handle to estopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
