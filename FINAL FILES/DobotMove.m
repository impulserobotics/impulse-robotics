function pos = DobotMove(posTrans)

global robot1;
%Inputs - position to move to 

if size(posTrans, 2) == 5
q = [posTrans(1), posTrans(2), posTrans(3), posTrans(4), posTrans(5)]
robot1.plot(q);
pos = getpos(robot1);
else
    
x = posTrans(1);
y = posTrans(2);
z = posTrans(3);
steps = 50;   
q = zeros(1,5);
% q =  [0,pi/4,pi/2,pi/4,0]
q1 = robot1.getpos();                                                    %sawywer1 current position
    clc;
    T2 = transl(x,y,z);                                      % Define a translation matrix            
    q2 = robot1.ikcon(T2,q1);                                   % Use inverse kinematics to get the joint angles
    qdeg = rad2deg(q2)
    qMatrix2 = jtraj(q1,q2,steps);
    
    robot1.plot(qMatrix2);
    
end
end
