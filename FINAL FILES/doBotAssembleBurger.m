function doBotAssembleBurger
clear all;
clc;
rosshutdown;
pause(0.5);
rosinit('138.25.249.246');

cartSvc = rossvcclient('dobot_magician/cart_pos');
cartMsg = rosmessage(cartSvc);
pause(1);

 msg2 = rosmessage('dobot_magician/SetPumpRequest')
  pause(1);
 eeSrv = rossvcclient('dobot_magician/pump')
  pause(1);
 msg2.Pump = 0;
 eeSrv.call(msg2)
 pause(1);
% %
posMid();

function posMid ()
cartMsg.Pos.X = (0.21839);
cartMsg.Pos.Y = (0.01394);
cartMsg.Pos.Z = (0.01561);
cartSvc.call(cartMsg);
pause(0.5);
end

function pos (x,y,z)
cartMsg.Pos.X = (x);
cartMsg.Pos.Y = (y);
cartMsg.Pos.Z = (z);
cartSvc.call(cartMsg);
pause(0.5);
end


function pump(sw)
msg2.Pump = sw;
eeSrv.call(msg2)
pause(0.5);
end

pos(0.250, -0.0975, -0.06679);
pump(1);
pos(0.252, -0.085, -0.034);
pos(0.252, -0.085, 0.1568);
pos(0.23211, 0.017944, -0.01);
pos(0.2057, 0.1667, 0.156825);
pos(0.2057, 0.1667, -0.034);

pump(0);
posMid();
end
