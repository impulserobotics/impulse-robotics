function ColDet
global robot1;
q = zeros(1,5);
centerpnt = [0.25,0.175,-0.05];
side = 0.1;
plotOptions.plotFaces = true;
[vertex,faces,faceNormals] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);
axis equal
camlight

tr = zeros(4,4,robot1.n+1);
tr(:,:,1) = robot1.base;
L = robot1.links;
for i = 1 : robot1.n
tr(:,:,i+1) = tr(:,:,i) * trotz(q(i)+L(i).offset) * transl(0,0,L(i).d) * transl(L(i).a,0,0) * trotx(L(i).alpha);
end


% Go through each link and also each triangle face
for i = 1 : size(tr,5)-1    
for faceIndex = 1:size(faces,1)
vertOnPlane = vertex(faces(faceIndex,1)',:);
[intersectP,check] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,tr(1:5,6,i)',tr(1:5,6,i+1)'); 
if check == 1 && IsIntersectionPointInsideTriangle(intersectP,vertex(faces(faceIndex,:)',:))
plot3(intersectP(1),intersectP(2),intersectP(3),intersectP(4),intersectP(5),'g*');
 display('Intersection');
end
end    
end


qWaypoints = [0,0,0,0,0;deg2rad(-83.7),deg2rad(18.0),deg2rad(-74),deg2rad(10.8),0];
qMatrix = InterpolateWaypointRadians(qWaypoints,deg2rad(10));
robot1.animate(qMatrix);

 
%Go through until there are no step sizes larger than 1 degree
q1 = [deg2rad(-83.7),deg2rad(18.0),deg2rad(-74),deg2rad(10.8),0];
q2 = [deg2rad(-135),deg2rad(11.9),deg2rad(-70.9),0,0];
steps = 2;
while ~isempty(find(1 < abs(diff(rad2deg(jtraj(q1,q2,steps)))),1))
steps = steps + 1;
end
qMatrix = jtraj(q1,q2,steps);

result = true(steps,1);
for i = 1: steps
result(i) = IsCollision(robot1,qMatrix(i,:),faces,vertex,faceNormals,false);
%robot1.animate(qMatrix(i,:));
end


if IsCollision(robot1,qMatrix,faces,vertex,faceNormals)
    error('Collision detected!!');
else
    display('No collision found');
end
% 

