function PlotEnv

%% Set Positions
basePos = [0 0 -0.1];                                                         %protective bunbase position
BeefBoxPos = [0.15 0.15 -0.05];                                             %beef box postion                                                    
BunBaseBoxPos = [0 0.15 -0.05];                                      %bunbase box position
BunCrownBoxPos = [0.15 -0.15 -0.05];                                         %bun crown box position
FinishedBoxPos = [0 -0.15 -0.05];                                          %finished items box position
ketchupPos = [-0.001 -0.156 0.302];                                                       
mustardPos = [0.3 -1.97 0.5]; 

 testPos = [0 0 0]; 

base = transl(basePos);                                                     %translate of basee position
BeefBox = transl(BeefBoxPos);                                         %translate of beef box postion                                                 
BunBaseBox = transl(BunBaseBoxPos);                             %translate of bunbase box position
BunCrownBox = transl(BunCrownBoxPos);                                   %translate of crown box position
finishedBox = transl(FinishedBoxPos);                                       %translate of finished items box position

%% plot cage
function  CagePlot()
  [f,v,data] = plyread('fullbase.ply','tri');
  vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ base(1,4),v(:,2) + base(2,4), v(:,3) + base(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  
  camlight;
  axis equal;
end

%% Plot Beef box
function  BeefBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BeefBox(1,4),v(:,2) + BeefBox(2,4), v(:,3) + BeefBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

%% plot BunBase box
function  BunBaseBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BunBaseBox(1,4),v(:,2) + BunBaseBox(2,4), v(:,3) + BunBaseBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end
%% plot bun crown box
function  BunCrownBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BunCrownBox(1,4),v(:,2) + BunCrownBox(2,4), v(:,3) + BunCrownBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end
%% plot finished box
function  FinishedBoxPlot()
  [f,v,data] = plyread('tray.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ finishedBox(1,4),v(:,2) + finishedBox(2,4), v(:,3)+ finishedBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

%% plot bunBase
function bunBasePlot()
  [f,v,data] = plyread('bun.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BunBaseBox(1,4),v(:,2) + BunBaseBox(2,4), v(:,3)+ BunBaseBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

%% plot bun crown
function BunCrownPlot()
  [f,v,data] = plyread('bun.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BunCrownBox(1,4),v(:,2) + BunCrownBox(2,4), v(:,3)+ BunCrownBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end
%% plot patty
function pattyPlot()
  [f,v,data] = plyread('patty.ply','tri');
   vertexColours = [data.vertex.red,data.vertex.green,data.vertex.blue]/255;
    trisurf(f,v(:,1)+ BeefBox(1,4),v(:,2) + BeefBox(2,4), v(:,3)+ BeefBox(3,4),'FaceVertexCData',vertexColours);
  colormap(parula(10));
  axis equal;
end

%%
   function LightCurtainPlot()
    
    v1=[-0.4,0.4 0 ],
    v2=[-0.4,-0.4,0],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;    
        
    v1=[-0.4,0.4 0.1 ],
    v2=[-0.4,-0.4,0.1],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;
    
    v1=[-0.4,0.4 0.2 ],
    v2=[-0.4,-0.4,0.2],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;
    
    v1=[-0.4,0.4 0.3 ],
    v2=[-0.4,-0.4,0.3],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;  
    
    %%%%%
        v1=[0.4,0.4 0 ],
    v2=[0.4,-0.4,0],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;    
        
    v1=[0.4,0.4 0.1 ],
    v2=[0.4,-0.4,0.1],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;
    
    v1=[0.4,0.4 0.2 ],
    v2=[0.4,-0.4,0.2],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;
    
    v1=[0.4,0.4 0.3 ],
    v2=[0.4,-0.4,0.3],
    v=[v2;v1];
    plot3(v(:,1),v(:,2),v(:,3),'r');
    hold on;  
    %%%%%%%%%%%%%%
    
    end

 %% PLOT ENVIRONMENT    
     CagePlot();                             %plot cage
%     StagePlot();                            %plot stage
     BeefBoxPlot();                       %plot circuit box
     BunBaseBoxPlot();                 %plot bottom housing box
     BunCrownBoxPlot();                    %plot top housing box
     FinishedBoxPlot();                      %plot fimished itmes box
     bunBasePlot();
     BunCrownPlot();
     pattyPlot();
     LightCurtainPlot();

end