function Obstruction()

% After saving in blender then load the triangle mesh
[f,v,data] = plyread('human.ply','tri');

% Get vertex count
monkeyVertexCount = size(v,1);

% Move center point to origin
% midPoint = sum(v)/monkeyVertexCount
midPoint = [0.7 -0.2 -0.1]; 

monkeyVerts = v - repmat(midPoint,monkeyVertexCount,1);

% Create a transform to describe the location (at the origin, since it's centered
monkeyPose = eye(4);

% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

% Then plot the trisurf
monkeyMesh_h = trisurf(f,monkeyVerts(:,1),monkeyVerts(:,2), monkeyVerts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');


%% Then move the object once using Robot Toolbox transforms and without replot
% axis([-10,10,-10,10,-2,2]);

% Move forwards (facing in -y direction)
forwardTR = makehgtform('translate',[0.01,0,0]);

% Random rotate about Z
% randRotateTR = makehgtform('zrotate',(rand-0.5)* 10 * pi/180);

% Move the pose forward and a slight and random rotation
monkeyPose = monkeyPose * forwardTR;
updatedPoints = [monkeyPose * [monkeyVerts,ones(monkeyVertexCount,1)]']';  

% Now update the Vertices
monkeyMesh_h.Vertices = updatedPoints(:,1:3);

%% Now move it many times
global robot1
for i = 1:15

q = [i/10, 0.1, 0.1, 0.1, 0.1]
robot1.plot(q);
    

    % Move forward then random rotation
    monkeyPose = monkeyPose * forwardTR

    % Transform the vertices
    updatedPoints = [monkeyPose * [monkeyVerts,ones(monkeyVertexCount,1)]']';
    
    % Update the mesh vertices in the patch handle
    monkeyMesh_h.Vertices = updatedPoints(:,1:3);
    drawnow();   
end

pause;



