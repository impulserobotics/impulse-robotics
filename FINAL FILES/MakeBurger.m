
function MakeBurger()

global robot1;

%%pick bun base,ketchup,mustard,come back to finish pos
qWaypoints = [deg2rad(0),deg2rad(0),deg2rad(0),deg2rad(0),0;
             deg2rad(35.1),deg2rad(37.4),deg2rad(-83.5),deg2rad(48.6),0;
             deg2rad(32.4),deg2rad(16.2),deg2rad(-73),deg2rad(61.2),0; 
             deg2rad(35.1),deg2rad(37.4),deg2rad(-83.5),deg2rad(48.6),0;
             deg2rad(37.8),deg2rad(37.4),deg2rad(-16.2),deg2rad(151),0;
             deg2rad(-2.7),deg2rad(37.4),deg2rad(-16.2),deg2rad(151),0;
             deg2rad(78.3),deg2rad(40),deg2rad(-95),deg2rad(54),0;
             deg2rad(78.3),deg2rad(23),deg2rad(-95),deg2rad(66.6),0];
qMatrix = InterpolateWaypointRadians(qWaypoints,deg2rad(3));
    robot1.plot(qMatrix);
    
pause(1);
    
 %%pick meet patty and come back to finish pos 
qWaypoints = [deg2rad(78.3),deg2rad(23),deg2rad(-95),deg2rad(66.6),0;
              deg2rad(78.3),deg2rad(40),deg2rad(-95),deg2rad(54),0;
              deg2rad(12),deg2rad(40),deg2rad(-95),deg2rad(54),0;
              deg2rad(-27),deg2rad(37.4),deg2rad(-83.5),deg2rad(48.6),0;              
              deg2rad(-32.4),deg2rad(16.2),deg2rad(-73),deg2rad(61.2),0;
              deg2rad(-27),deg2rad(37.4),deg2rad(-83.5),deg2rad(48.6),0;
              deg2rad(12),deg2rad(40),deg2rad(-95),deg2rad(54),0;
               deg2rad(78.3),deg2rad(40),deg2rad(-95),deg2rad(54),0;
              deg2rad(78.3),deg2rad(23),deg2rad(-95),deg2rad(66.6),0];
qMatrix = InterpolateWaypointRadians(qWaypoints,deg2rad(3));
    robot1.plot(qMatrix);
    
pause(1);
    
  %%pick bun base and come back to finish pos
qWaypoints = [deg2rad(78.3),deg2rad(23),deg2rad(-95),deg2rad(66.6),0;
              deg2rad(78.3),deg2rad(40),deg2rad(-95),deg2rad(54),0;
              deg2rad(-75.6),deg2rad(40),deg2rad(-95),deg2rad(54),0;
              deg2rad(-75.6),deg2rad(23),deg2rad(-95),deg2rad(66.2),0;
              deg2rad(-75.6),deg2rad(40),deg2rad(-95),deg2rad(54),0;
              deg2rad(78.3),deg2rad(40),deg2rad(-95),deg2rad(54),0;
              deg2rad(78.3),deg2rad(23),deg2rad(-95),deg2rad(66.6),0];
qMatrix = InterpolateWaypointRadians(qWaypoints,deg2rad(3));
    robot1.plot(qMatrix);



end
